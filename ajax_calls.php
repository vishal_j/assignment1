<?php
$con = mysqli_connect("localhost", "root", "", "assignment");
if(mysqli_connect_errno()){
    echo "failed to connect". mysqli_connect_error();
}
session_start();
if(isset($_SESSION['user_id'])){
    $user_id = $_SESSION['user_id'];
    $user_name = $_SESSION['user_name'];
}

$data = (file_get_contents("php://input"));
$data = json_decode($data, true);
if(isset($data['email'])){
    $email = $data['email'];
    $password = $data['password'];

    if (empty($email))
      $errors['email'] = 'Email is required.';
    if (empty($password))
      $errors['password'] = 'Password is required.';

    $select = $con->query("SELECT * FROM `users` WHERE `email_id`='$email' AND `password`='$password'");
    if($select->num_rows == 0){
        $errors['login'] = 'wrong combination';
    }
    else{
        while($row = $select->fetch_assoc()){
            $_SESSION['user_id'] = $row['user_id'];
            $_SESSION['user_name'] = $row['name'];
        }
    }
    if (!empty($errors)) {
      $data1['errors']  = $errors;
    } else {
      $data1['message'] = 'Form data is going well';
    }
    if(isset($errors))
        echo json_encode($data1);
}
else if(isset($_GET['fetchData'])){
    $select = $con->query("SELECT * FROM `library` WHERE `user_id`='$user_id'");
    while($row = $select->fetch_assoc()){
        $data[] = $row;
    }
    $data2 = array();
    $data2['username'] = $user_name;
    $data2['userLib'] = $data;
    echo json_encode($data2);
}
else if(isset($data['youtubeurl'])){
    $title = $data['title'];
    $youtubeurl = $data['youtubeurl'];
    if(preg_match('/^http.+youtube\.com\/watch\?v=(.+)/', $youtubeurl, $verify)){
        $video_id = $verify[1];
    }
    else{
        $video_id = $youtubeurl;
    }
    $stime = $data['stime'];
    $etime = $data['etime'];
    
    $data = array();
    if(empty($title) || empty($youtubeurl) || empty($stime) || empty($etime)){
        $errors['error'] = "All fields are mandatory";
    }
    else{
        $select = $con->query("SELECT * FROM `library` WHERE `user_id`='$user_id' AND `yt_url`='$youtubeurl'");
        if($select->num_rows == 0){
            $insert = $con->prepare("INSERT INTO `library`(`user_id`, `title`, `yt_url`, `start`, `end`) VALUES(?, ?, ?, ?, ?)");
            $insert->bind_param('issss', $user_id, $title, $video_id, $stime, $etime);
            if($insert->execute()){
                $data['success'] = 'Video updated';
            }
            else{
                $errors['error'] = 'Something went wrong while uploading details.';
            }
        }
        else{
            $errors['error'] = 'You already have this video in your library.';
        }
    }
    if(isset($errors)){
        $data['errors'] = $errors;
    }
    echo json_encode($data);
}
?>